var prices=["Цена: 10 <sup>∞</sup> Рублей","Цена: очень много Рублей","Цена: для вас копейки","Цена: 3200 рублей"];
$("document").ready(function () {
	init();
	
    if($(window).width()<800){
	$("textarea").attr({cols:"40"});
	
		$(".section").attr({style:"display:all;"});
        $("form").find("img").attr({style:"width:100%;height:400px;margin:0;"});
		$(".main").css("padding","0");
    }
	
    else{
		$(".section").attr({style:"display:none;"});
        $("form").find("img").attr({style:"width:400px;height:400px;"});
		$(".main").css("padding","20");
    }

    $(window).resize(function () {
		if($(window).width()<800){
		$("textarea").attr({cols:"40"});
			$(".section").attr({style:"display:all;"});
           $("form").find("img").attr({style:"width:100%;height:400px;"});
			$(".main").css("padding","0");
        }
        else{
			$(".section").attr({style:"display:all;"});
            $("form").find("img").attr({style:"width:400px;height:400px;margin:0;"});
			$(".main").css("padding","20");
			$("textarea").attr({cols:"80"});
        }
    });
	
	$("select").change(function(){
		str=$( "select option:selected" ).attr("id");
		$("form").find("img").attr({src:("../images/hotel"+str+".jpg")});
		$("#price h2").text(prices[str-1]);
	});
});